package ru.kazakov.iteco;

public class Project {
    String name;
    private String projectInfo;

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getProjectInfo() {
        return projectInfo;
    }

    public void setProjectInfo(String projectInfo) {
        this.projectInfo = projectInfo;
    }
}
