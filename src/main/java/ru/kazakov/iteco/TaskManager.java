package ru.kazakov.iteco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskManager {
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    static LinkedHashMap<String, Project> projects = new LinkedHashMap<>();
    static LinkedHashMap<String,Task> tasks = new LinkedHashMap<>();
    private static String separator = System.lineSeparator();


    public static void main(String[] args) throws IOException {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = enterName().trim().toLowerCase();
        while (!command.equals("manager-close")) {
            switch (command) {
                case "help"           : help();          break;
                case "project-create" : createProject(); break;
                case "project-get"    : getProject();    break;
                case "project-update" : updateProject(); break;
                case "project-remove" : removeProject(); break;
                case "project-list"   : listProject();   break;
                case "project-clear"  : clearProject();  break;
                case "task-create"    : createTask();    break;
                case "task-get"       : getTask();       break;
                case "task-update"    : updateTask();    break;
                case "task-remove"    : removeTask();    break;
                case "task-list"      : listTask();   break;
                case "task-clear"     : clearTask();  break;
                default               :
                    System.out.println("Command doesn't exist. Use \"help\" to show all commands." + separator);
            }
            command = enterName().trim().toLowerCase();
        }
        System.out.println("*** MANAGER CLOSED ***");
    }

    private static void createProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            System.out.println("[NOT CREATED]" + separator + "Project with this name is already exists. Use another project name.");
        } else {
            projects.put(projectName, new Project(projectName));
            System.out.println("[CREATED]" + separator + "Project successfully created!");
        }
        System.out.print(separator);
    }

    private static void getProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            if (projects.get(projectName).getProjectInfo() == null || projects.get(projectName).getProjectInfo().length() == 0) {
                System.out.println("Project is empty. Use \"project-update\" to update this project.");
            }
            System.out.println("[PROJECT: " + projectName + "]");
            System.out.println(projects.get(projectName).getProjectInfo());
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    private static void updateProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            System.out.println("Use \"-save\" to finish entering, and save information.");
            System.out.println("ENTER PROJECT INFORMATION");
            StringBuilder builder = new StringBuilder();
            String temp = "";
            while (true){
                temp = reader.readLine().trim();
                if (temp.equals("-save")) {
                    break;
                }
                builder.append(temp);
                builder.append(separator);
            }
            projects.get(projectName).setProjectInfo(builder.toString());
            System.out.println("[UPDATED]" + separator + "Project successfully updated!");
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    private static void removeProject() throws IOException {
        System.out.println("ENTER PROJECT NAME:");
        String projectName = enterName();
        if (projects.containsKey(projectName)) {
            projects.remove(projectName);
            System.out.println("[REMOVED]" + separator + "Project successfully removed!");
        } else {
            System.out.println("Project with this name doesn't exist. Use \"project-list\" to show all projects.");
        }
        System.out.print(separator);
    }

    private static void listProject() {
        if (projects.isEmpty()) {
            System.out.println("Project list is empty. Use \"project-create\" to create project.");
        } else {
            int counter = 1;
            System.out.println("[PROJECTS LIST]");
            for (Map.Entry<String, Project> entry : projects.entrySet()
            ) {
                System.out.println(counter + ". " + entry.getKey());
                counter++;
            }
        }
        System.out.print(separator);
    }

    private static void clearProject() {
        projects.clear();
        System.out.println("[ALL PROJECTS REMOVED]" + separator + "Projects successfully removed!" + separator);
    }

    private static void createTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            System.out.println("[NOT CREATED]" + separator + "Task with this name is already exists. Use another task name.");
        } else {
            tasks.put(taskName, new Task(taskName));
            System.out.println("[CREATED]" + separator + "Task successfully created!");
        }
        System.out.print(separator);
    }

    private static void getTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            if (tasks.get(taskName).getTaskInfo() == null || tasks.get(taskName).getTaskInfo().length() == 0) {
                System.out.println("Task is empty. Use \"task-update\" to update this task.");
            }
            System.out.println("Task: " + taskName);
            System.out.println(tasks.get(taskName).getTaskInfo());
        } else {
            System.out.println("Tasks with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    private static void updateTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            System.out.println("Use \"-save\" to finish entering, and save information.");
            System.out.println("ENTER TASK INFORMATION");
            StringBuilder builder = new StringBuilder();
            String temp = "";
            while (true) {
                temp = reader.readLine().trim();
                if (temp.equals("-save")) {
                    break;
                }
                builder.append(temp);
                builder.append(separator);
            }
            tasks.get(taskName).setTaskInfo(builder.toString());
            System.out.println("[UPDATED]" + separator + "Task successfully updated!");
        } else {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    private static void removeTask() throws IOException {
        System.out.println("ENTER TASK NAME:");
        String taskName = enterName();
        if (tasks.containsKey(taskName)) {
            tasks.remove(taskName);
            System.out.println("[REMOVED]" + separator + "Task successfully removed!");
        } else {
            System.out.println("Task with this name doesn't exist. Use \"task-list\" to show all tasks.");
        }
        System.out.print(separator);
    }

    private static void listTask() {
        if (tasks.isEmpty()) {
            System.out.println("Tasks list is empty. Use \"task-create\" to create task.");
        } else {
            int counter = 1;
            System.out.println("[TASK LIST]");
            for (Map.Entry<String, Task> entry : tasks.entrySet()
            ) {
                System.out.println(counter + ". " + entry.getKey());
                counter++;
            }
        }
        System.out.print(separator);
    }

    private static void clearTask() {
        tasks.clear();
        System.out.println("[ALL TASKS REMOVED]" + separator + "Tasks successfully removed!" + separator);
    }

    private static String enterName() throws IOException {
        String name = reader.readLine();
        while (name.length() == 0) {
            name = reader.readLine();
        }
        return name.trim();
    }

    private static void help() {
        System.out.print("help: Show all commands." + separator +
                "project-create: Create new project." + separator +
                "project-get: Show all project information." + separator +
                "project-update: Update project information." + separator +
                "project-remove: Remove project." + separator +
                "project-list: Show all projects." + separator +
                "project-clear: Remove all projects." + separator +
                "task-create: Create new task." + separator +
                "task-get: Show all task information." + separator +
                "task-update: Update task information." + separator +
                "task-remove: Remove task." + separator +
                "task-list: Show all tasks." + separator +
                "task-clear: Remove all tasks." + separator +
                "-save: Save all entered information to project or task." + separator +
                "manager-close: Close task manager.");
        System.out.print(separator);
    }
}
